﻿// ---------------------------------------------------------------------------
// <copyright file="ThreeAndFiveDivisionRuleTests.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the test cases for ThreeAndFive Divisibles.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication.BusinessLayer.Tests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// test cases for numbers which are divisible by 3 and 5
    /// </summary>
    public class ThreeAndFiveDivisionRuleTests
    {
        /// <summary>
        /// Local Variable for ThreeAndFive Division.
        /// </summary>
        private ThreeAndFiveDivisionRule threeAndFiveDivisionRule;

        /// <summary>
        /// To Initialize the objects used in the test cases.
        /// </summary>
        [SetUp]
        public void Initialise()
        {
            this.threeAndFiveDivisionRule = new ThreeAndFiveDivisionRule();
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Pass -- Thursday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_15_Thursday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Fizz + " " + FizzBuzzResources.Buzz, this.threeAndFiveDivisionRule.DivisionProcess(15, DayOfWeek.Thursday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Pass -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_15_Wednesday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Wizz + " " + FizzBuzzResources.Wuzz, this.threeAndFiveDivisionRule.DivisionProcess(15, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Fail -- Thursday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_15_Thursday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Wizz + " " + FizzBuzzResources.Wuzz, this.threeAndFiveDivisionRule.DivisionProcess(15, DayOfWeek.Thursday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Fail -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_15_Wednesday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Fizz + " " + FizzBuzzResources.Buzz, this.threeAndFiveDivisionRule.DivisionProcess(15, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Pass -- Thursday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_16_Thursday_Pass()
        {
            Assert.AreEqual("16", this.threeAndFiveDivisionRule.DivisionProcess(16, DayOfWeek.Thursday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Pass -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_16_Wednesday_Pass()
        {
            Assert.AreEqual("16", this.threeAndFiveDivisionRule.DivisionProcess(16, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Fail -- Thursday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_16_Thursday_Fail()
        {
            Assert.AreNotEqual("16", this.threeAndFiveDivisionRule.DivisionProcess(6, DayOfWeek.Thursday));
        }

        /// <summary>
        /// Test Case for ThreeAndFive Division. Fail --Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeAndFiveDivision_16_Wednesday_Fail()
        {
            Assert.AreNotEqual("16", this.threeAndFiveDivisionRule.DivisionProcess(6, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// To Dispose the used Objects.
        /// </summary>
        [TearDown]
        public void Dispose()
        {
            this.threeAndFiveDivisionRule = null;
        }
    }
}
