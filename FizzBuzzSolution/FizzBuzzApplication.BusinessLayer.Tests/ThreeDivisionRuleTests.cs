﻿// ---------------------------------------------------------------------------
// <copyright file="ThreeDivisionRuleTests.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the test cases for Three Divisibles.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication.BusinessLayer.Tests
{
    using System;
    using NUnit.Framework;

    /// <summary>
    /// Test Class for ThreeDivision numbers.
    /// </summary>
    [TestFixture]
    public class ThreeDivisionRuleTests
    {
        /// <summary>
        /// Local Variable for Three Division.
        /// </summary>
        private ThreeDivisionRule threeDivisionRule;

        /// <summary>
        /// To Initialize the objects used in the test cases.
        /// </summary>
        [SetUp]
        public void Initialise()
        {
            this.threeDivisionRule = new ThreeDivisionRule();
        }

        /// <summary>
        /// Test Case for Three Division. Pass -- Tuesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Tuesday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Fizz, this.threeDivisionRule.DivisionProcess(3, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Division. Pass -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Wednesday_Pass()
        {
            Assert.AreEqual(FizzBuzzResources.Wizz, this.threeDivisionRule.DivisionProcess(3, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail -- Tuesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Tuesday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Wizz, this.threeDivisionRule.DivisionProcess(3, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_3_Wednesday_Fail()
        {
            Assert.AreNotEqual(FizzBuzzResources.Fizz, this.threeDivisionRule.DivisionProcess(3, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Pass  -- Tuesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Tuesday_Pass()
        {
            Assert.AreEqual("4", this.threeDivisionRule.DivisionProcess(4, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Pass  -- Wednesday
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Wednesday_Pass()
        {
            Assert.AreEqual("4", this.threeDivisionRule.DivisionProcess(4, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Tuesday_Fail()
        {
            Assert.AreNotEqual("4", this.threeDivisionRule.DivisionProcess(5, DayOfWeek.Tuesday));
        }

        /// <summary>
        /// Test Case for Three Divisible. Fail
        /// </summary>
        [Test]
        public void Test_ThreeDivisible_4_Wednesday_Fail()
        {
            Assert.AreNotEqual("4", this.threeDivisionRule.DivisionProcess(5, DayOfWeek.Wednesday));
        }

        /// <summary>
        /// To Dispose the used Objects.
        /// </summary>
        [TearDown]
        public void Dispose()
        {
            this.threeDivisionRule = null;
        }
    }
}
