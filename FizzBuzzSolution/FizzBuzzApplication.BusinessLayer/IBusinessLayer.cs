﻿// ---------------------------------------------------------------------------
// <copyright file="IBusinessLayer.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        IBusinessLayer interface for main BusinessLayer.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// interface for generating sequence in business layer
    /// </summary>
    public interface IBusinessLayer
    {
        /// <summary>
        /// Gets or sets Property to set Current Day of the week.
        /// </summary>
        DayOfWeek CurrentDay { get; set; }

        /// <summary>
        /// generates List string fizz buzz sequence
        /// </summary>
        /// <param name="inputNumber">user input</param>
        /// <returns>fizz buzz sequence</returns>
        List<string> GenerateFizzBuzzSequence(int inputNumber);
    }
}