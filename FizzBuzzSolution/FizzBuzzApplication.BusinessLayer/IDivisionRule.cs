﻿// ---------------------------------------------------------------------------
// <copyright file="IDivisionRule.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        IDivisionRule interface for division process.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.BusinessLayer
{
    using System;

    /// <summary>
    /// Base interface for division process
    /// </summary>
    public interface IDivisionRule
    {
        /// <summary>
        /// parent division interface
        /// </summary>
        /// <param name="number">user input</param>
        /// <param name="day">day of week</param>
        /// <returns>string result</returns>
        string DivisionProcess(int number, DayOfWeek day);
    }
}
