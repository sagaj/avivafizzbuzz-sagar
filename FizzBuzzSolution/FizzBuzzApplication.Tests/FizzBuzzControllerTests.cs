﻿// ---------------------------------------------------------------------------
// <copyright file="FizzBuzzControllerTests.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the test cases for FizzBuzzController class.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using BusinessLayer;
    using Controllers;
    using Models;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// class used to test FizzBuzzControllerTests.
    /// </summary>
    public class FizzBuzzControllerTests
    {
        /// <summary>
        /// Declaring Repository.
        /// </summary>
        private IBusinessLayer fizzbuzzbusniessLayer = null;

        /// <summary>
        /// Declaring FizzBuzz Controller.
        /// </summary>
        private FizzBuzzController fizzbuzzcontroller;

        /// <summary>
        /// Declaring Local Mock variables for Custom Repository.
        /// </summary>
        private Mock<IBusinessLayer> mockbunisessLayer;

        /// <summary>
        /// Declaring Local variables for FizzBuzz Model.
        /// </summary>
        private FizzBuzzModel modelInstance;

        /// <summary>
        /// page number input
        /// </summary>
        private int? pageNumber = 1;

        /// <summary>
        /// To Initialize local variables used in the test cases.
        /// </summary>
        [SetUp]
        public void Initialize()
        {
            // if some number not passed in input, in controller we will get an exception
            this.modelInstance = new FizzBuzzModel { Input = 2 };
            this.mockbunisessLayer = new Mock<IBusinessLayer> { CallBase = true };
            this.mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(It.IsAny<int>())).Returns(
                 new List<string> 
                 { 
                        "1",
                        "2",
                        "fizz",
                        "4",
                        "buzz"  
            });
            this.fizzbuzzcontroller = new FizzBuzzController(this.mockbunisessLayer.Object);
        }

        /// <summary>
        /// Test Case to check Constructor.
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_Constructor_Pass()
        {
            this.fizzbuzzcontroller = new FizzBuzzController(this.fizzbuzzbusniessLayer);
            Assert.AreEqual(typeof(FizzBuzzController), this.fizzbuzzcontroller.GetType());
        }

        /// <summary>
        /// Test Case to check Constructor.
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_Constructor_Fail()
        {
            this.fizzbuzzcontroller = new FizzBuzzController(this.fizzbuzzbusniessLayer);
            Assert.AreNotEqual(typeof(IBusinessLayer), this.fizzbuzzcontroller.GetType());
        }

        /// <summary>
        /// Test Case to check for View Name. Pass
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_ViewName_Pass()
        {
            // Mocking GetFizzBuzzcollection in SetUp.
            var actual = this.fizzbuzzcontroller.FizzBuzzHome(this.pageNumber) as ViewResult;
            Assert.AreEqual("FizzBuzzHome", actual.ViewName);
        }

        /// <summary>
        /// Test Case to check for View Name. Fail
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_ViewName_Fail()
        {
            // Mocking GetFizzBuzzcollection in SetUp.
            var actual = this.fizzbuzzcontroller.FizzBuzzHome(this.pageNumber) as ViewResult;
            Assert.AreNotEqual("Home", actual.ViewName);
        }

        /// <summary>
        /// Test Case to check for FizzBuzz list count. Pass
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_FizzBuzzList_Count_Pass()
        {
            // Mocking GetFizzBuzzcollection in SetUp.
            ViewResult actual = this.fizzbuzzcontroller.FizzBuzzHome(this.modelInstance, this.pageNumber) as ViewResult;
            Assert.AreEqual(5, ((FizzBuzzModel)actual.Model).FizzBuzzList.ToList().Count);
        }

        /// <summary>
        /// Test Case to check for FizzBuzz list count. Fail
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_FizzBuzzList_Count_Fail()
        {
            // Mocking GetFizzBuzzcollection in SetUp.
            ViewResult actual = this.fizzbuzzcontroller.FizzBuzzHome(this.modelInstance, this.pageNumber) as ViewResult;
            Assert.AreNotEqual(7, ((FizzBuzzModel)actual.Model).FizzBuzzList.ToList().Count);
        }

        /// <summary>
        /// Test Case to check for FizzBuzz list on Wednesday. Pass
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_FizzBuzzList_Wednesday_Pass()
        {
            // Mocking GetFizzBuzzcollection.
            this.mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(It.IsAny<int>())).Returns(
                new List<string> 
                { 
                        "1",
                        "2",
                        "wizz",
                        "4",
                        "wuzz"  
            });
            this.fizzbuzzcontroller = new FizzBuzzController(this.mockbunisessLayer.Object);
            var actual = this.fizzbuzzcontroller.FizzBuzzHome(this.modelInstance, this.pageNumber) as ViewResult;
            Assert.Contains(FizzBuzzResources.Wizz, ((FizzBuzzModel)actual.Model).FizzBuzzList.ToList());
        }

        /// <summary>
        /// Test Case to check for FizzBuzz list on Wednesday. Fail
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_FizzBuzzList_Wednesday_Fail()
        {
            this.mockbunisessLayer.Setup(m => m.GenerateFizzBuzzSequence(It.IsAny<int>())).Returns(
                new List<string>
                { 
                        "1",
                        "2",
                        "wizz",
                        "4",
                        "wuzz"  
            });
            this.fizzbuzzcontroller = new FizzBuzzController(this.mockbunisessLayer.Object);
            var actual = this.fizzbuzzcontroller.FizzBuzzHome(this.modelInstance, this.pageNumber) as ViewResult;
            Assert.IsFalse(((FizzBuzzModel)actual.Model).FizzBuzzList.ToList().Contains(FizzBuzzResources.Fizz));
        }

        /// <summary>
        /// Test Case to check for FizzBuzz list on Monday. Pass
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_FizzBuzzList_Monday_Pass()
        {
            // Mocking GetFizzBuzzcollection in SetUp.
            var actual = this.fizzbuzzcontroller.FizzBuzzHome(this.modelInstance, this.pageNumber) as ViewResult;
            Assert.Contains(FizzBuzzResources.Fizz, ((FizzBuzzModel)actual.Model).FizzBuzzList.ToList());
        }

        /// <summary>
        /// Test Case to check for FizzBuzz list on Wednesday. Fail
        /// </summary>
        [Test]
        public void Test_FizzBuzzController_FizzBuzzList_Monday_Fail()
        {
            // Mocking GetFizzBuzzcollection in SetUp.
            var actual = this.fizzbuzzcontroller.FizzBuzzHome(this.modelInstance, this.pageNumber) as ViewResult;
            Assert.IsFalse(((FizzBuzzModel)actual.Model).FizzBuzzList.ToList().Contains(FizzBuzzResources.Wizz));
        }

        /// <summary>
        /// To Dispose the objects once test cases ran.
        /// </summary>
        [TearDown]
        public void Dispose()
        {
            this.modelInstance = null;
            this.mockbunisessLayer = null;
            this.fizzbuzzcontroller = null;
            this.fizzbuzzbusniessLayer = null;
        }
    }
}
