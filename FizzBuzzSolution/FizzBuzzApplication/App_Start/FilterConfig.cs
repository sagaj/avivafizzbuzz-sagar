﻿// ---------------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        FilterConfig class for filtering
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication
{
    using System.Web.Mvc;

    /// <summary>
    /// FilterConfig used for filtering purpose
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// used for RegisterGlobalFilters
        /// </summary>
        /// <param name="filters">GlobalFilterCollection for filtering</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}