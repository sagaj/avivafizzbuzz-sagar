﻿// ---------------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
////<summary>
//  RouteConfig for routing purpose.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication
{
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// RouteConfig used for routing purpose
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Used for RegisterRoutes
        /// </summary>
        /// <param name="routes">Route Collection</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "FizzBuzz", action = "FizzBuzzHome", id = UrlParameter.Optional });
        }
    }
}