﻿// ---------------------------------------------------------------------------
// <copyright file="WebApiConfig.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
////<summary>
//  WebApiConfig.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication
{
    using System.Web.Http;

    /// <summary>
    /// used for <c>WebApi</c> configuration
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Used for Http Register 
        /// </summary>
        /// <param name="config">Http Configuration</param>
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });
        }
    }
}
