﻿// ---------------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains FizzBuzzController class.
// </summary>
// ---------------------------------------------------------------------------
namespace FizzBuzzApplication.Controllers
{
    using System;
    using System.Configuration;
    using System.Web.Mvc;
    using BusinessLayer;
    using Models;
    using PagedList;

    /// <summary>
    /// FizzBuzzController for generating fizz buzz sequence
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// private BusinessLayer interface
        /// </summary>
        private readonly IBusinessLayer businessLayer;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class
        /// </summary>
        /// <param name="ibusinessLayer">businessLayer object</param>
        public FizzBuzzController(IBusinessLayer ibusinessLayer)
        {
            this.businessLayer = ibusinessLayer;
        }

        /// <summary>
        /// Gets page size
        /// </summary>
        private int PageSize
        {
            get
            {
                var pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
                if (pageSize <= 0)
                {
                    pageSize = 20;
                }

                return pageSize;
            }
        }

        /// <summary>
        /// Gets or sets input value
        /// </summary>
        private int Input
        {
            get { return Convert.ToInt32(this.TempData["Input"]); }
            set { this.TempData["Input"] = value; }
        }

        /// <summary>
        /// used to return the FizzBuzz Home page
        /// </summary>
        /// <param name="pageNumber">page Number</param>
        /// <returns>FizzBuzzHome View</returns>
        [HttpGet]
        public ActionResult FizzBuzzHome(int? pageNumber)
        {
            FizzBuzzModel model = new FizzBuzzModel();
            if (ModelState.IsValid)
            {
                if (this.Input > 0)
                {
                    model = this.FetchFizzBuzzSequence(this.Input, pageNumber);
                }
            }

            return this.View("FizzBuzzHome", model);
        }

        /// <summary>
        /// used to return the generated FizzBuzz sequence to view
        /// </summary>
        /// <param name="model">FizzBuzzModel Model</param>
        /// <param name="pageNumber">Page Number</param>
        /// <returns>FizzBuzzHome View</returns>
        [HttpPost]
        public ActionResult FizzBuzzHome(FizzBuzzModel model, int? pageNumber)
        {
            if (ModelState.IsValid)
            {
                model = this.FetchFizzBuzzSequence(model.Input.Value, pageNumber);
                ModelState.Clear();
            }

            return this.View("FizzBuzzHome", model);
        }

        /// <summary>
        /// method to call BusinessLayer
        /// </summary>
        /// <param name="inputNumber">input number</param>
        /// <param name="pageNumber">page number</param>
        /// <returns>FizzBuzz Model</returns>
        private FizzBuzzModel FetchFizzBuzzSequence(int? inputNumber, int? pageNumber)
        {
            var fizzBuzzList = this.businessLayer.GenerateFizzBuzzSequence(inputNumber.Value);
            var pagenumber = pageNumber ?? 1;
            this.Input = inputNumber.Value;
            var model = new FizzBuzzModel { Input = inputNumber.Value, FizzBuzzList = fizzBuzzList.ToPagedList(pagenumber, this.PageSize) };
            return model;
        }
    }
}
