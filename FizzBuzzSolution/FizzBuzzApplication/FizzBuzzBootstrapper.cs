﻿// ---------------------------------------------------------------------------
// <copyright file="FizzBuzzBootstrapper.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        used for unity addition to container and resolving the classes.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using BusinessLayer;
    using Microsoft.Practices.Unity;
    using Unity.Mvc3;

    /// <summary>
    /// class used to store and retrieve objects of dependent interfaces
    /// </summary>
    public static class FizzBuzzBootstrapper
    {
        /// <summary>
        /// Gets or sets the GlobalUnityContainer
        /// </summary>
        public static IUnityContainer GlobalUnityContainer { get; set; }

        /// <summary>
        /// Start-up Method for Unity Container.
        /// </summary>
        public static void Initialise()
        {
            GlobalUnityContainer = BuildUnityContainer();
            DependencyResolver.SetResolver(new UnityDependencyResolver(GlobalUnityContainer));
        }

        /// <summary>
        /// used to register the interface and respective class.
        /// </summary>
        /// <returns>master container</returns>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // registering components with the container
            container.RegisterType<IDivisionRule, ThreeDivisionRule>("ThreeDivision");
            container.RegisterType<IDivisionRule, FiveDivisionRule>("FiveDivision");
            container.RegisterType<IDivisionRule, ThreeAndFiveDivisionRule>("ThreeAndFiveDivision");

            List<IDivisionRule> listDivisions = new List<IDivisionRule>();
            listDivisions.Add(container.Resolve<ThreeAndFiveDivisionRule>());
            listDivisions.Add(container.Resolve<ThreeDivisionRule>());
            listDivisions.Add(container.Resolve<FiveDivisionRule>());

            // Registering the IRepository interface and its child class.
            container.RegisterType<IBusinessLayer, BusinessLayer.BusinessLayer>(new InjectionConstructor(listDivisions));
            return container;
        }
    }
}