﻿// ---------------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains Global Level events.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    /// <summary>
    /// Base class from http application
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application_Start Method
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FizzBuzzBootstrapper.Initialise();
        }
    }
}