﻿// ---------------------------------------------------------------------------
// <copyright file="FizzBuzzModel.cs" company="TCS">
//     Copyright (c) TCS.  All rights reserved.
// </copyright>
// <summary>
//        File which contains the validations for the User Input.
// </summary>
// ---------------------------------------------------------------------------

namespace FizzBuzzApplication.Models
{
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    /// <summary>
    /// Class which holds user input and stores FizzBuzzList.
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets User input number.
        /// </summary>
        [Required(ErrorMessage = @"*Please enter {0}")]
        [Range(typeof(int), "1", "1000", ErrorMessage = @"Please enter {0} between {1} and {2}")]
        public int? Input { get; set; }

        /// <summary>
        /// Gets or sets FizzBuzzList.
        /// </summary>
        public IPagedList<string> FizzBuzzList { get; set; }
    }
}
